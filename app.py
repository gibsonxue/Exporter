from flask import Flask,jsonify
import os
from modules.sysinfo import NodeResource

app = Flask(__name__)

@app.route("/")
def hello():
    res = NodeResource(root_path=os.environ['CHROOT'],hostname=os.environ['HOSTNAME'])
    sys = {}
    sys['cpu'] = res.cpu_stat
    sys['disk'] = res.disk_stat
    sys['ram'] = res.memory_stat
    sys['data'] = res.data
    sys['hostname'] = res.hostname
    return jsonify(sys)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5001))
    app.run(debug=True,host='0.0.0.0',port=port)
